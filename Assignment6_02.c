#include <stdio.h>

int fibonacci(int n);
void parseNum(int num);

//main function
int main()
{
    int num;
    printf("This program is to print the Fibonacci sequence\n");
    printf("How many terms do you want?");
    scanf("%d",&num);
    printf("\n");
    parseNum(num);

    return 0;
}

/*function to parse numbers
  to the fibonacci function*/
void parseNum(int num)
{
    if (num<0)
    {
        return;
    }
    else
    {
        parseNum(num-1);
        printf("%d\n",fibonacci(num));
    }

}

//function to generate sequence
int fibonacci(int n)
{
    if (n==0 || n == 1)
    {
        return n;
    }
    else
    {
       return fibonacci(n-1)+fibonacci(n-2);
    }
}
