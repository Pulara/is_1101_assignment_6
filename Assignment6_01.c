#include <stdio.h>

void parseNum(int num);
void printRow(int n);

//main function
int main()
{
    int num;
    printf("Enter the pattern number: ");
    scanf("%d",&num);
    printf("\n");
    parseNum(num);

    return 0;
}

/*function to parse numbers
  to the printing function*/
void parseNum(int num)
{
    if (num == 0)
    {
        return;
    }
    else
    {
        parseNum(num - 1);
        printRow(num);
        printf("\n");
    }

}

//function to print rows
void printRow(int n)
{
    printf("%d",n);
    if (n == 1)
    {
        return;
    }
    else
    {
        printRow(n-1);
    }
}
